Step 1: To set up the JSON Server run the following command: npm install -g json-server.
Step 2: Start JSON Server. json-server --watch db.json --port 8000. This runs a local server on port 8000, and watches the db.json file for any changes.
